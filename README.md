# Furimo_Cloud Docs / How Furimo Cloud Works


## What files in this repo do??????????????????
`payments.txt` => Is file which stores payments that are done everyday at 00:00 | DUCO to Bidder,  TRX to miner

`offers.txt` => Is file which stores parameters of offer such as Miner username, Price per day, hash of offer, Miner TRX ADDR

hash of offer ~ is used to create something ID-like so u just type hash and buy offer

`duinocloud.py` => Main file that do job behind the website (Backend File)

### Template Directory files:

`form.html` => main file which is used for website displaying(frontend)


`versions.html` => website/html file which is used for defining states of software

`429.html` => website/html file which will show if u reach error 429 [too many requests error]


## How duinocloud/furimocloud works?
Well it will be easier to explain with scenarios that can be done with that program 

### `Creating mining point scenario`


**Alice decides to create mining point** (Mining place/acc where duco will be mined)

**Alice types account username** (Not existing account but new, which mining point will be mined on)

**Alice types her TRX address** (To make payments to this address)

**Alice types Price per day** (To pay amount of needed trx for hashing power)

**Alice types e-mail**(To register new  duino account with that email)


⬇️

**Alice decides to mine on** `Alice_Mining69Point`

⬇️

**Hashrate of**  `Alice_Mining69Point` **is displayed on website**

### `Buying mining point scenario`

**Bob see Alice offer on dashboard**

⬇️

**Bob decides to buy Alice offer**(To buy alice offer Bob needs (Price per day * 7) amount, because of week having 7 days)

⬇️


**Bob is copying offer hash**

**Bob is typing out his private key**(To make TRX payment)

**Bob is typing out his addr**(To make TRX payment, because to make payment it requires your TRX addr)

**Bob is typing out his DUCO username**(Because when u buy offer DUCO must go to some address)


⬇️

**Bob sucessfully bought offer of Alice**


⬇️

**Now At 00:00 Payments wil be made** | Duco to Bider(someone who bought offer), TRX to Miner


## What stuff is not implemented yet?

- [ ] If miner didnt mine anything in day amount that was payed for more days is refunded
- [ ] Offers age, what i mean by that u display offer for a few days and then its removed 
- [ ] Paying for whatever days u want aka If price per day is 3 TRX and u have 12 TRX u can buy offer for a 4 days (only if u want to pay more)

## Contact for Questions and other stuff

email: `remusmaluss@gmail.com`

discord: `%'#5584`


## Donations and affilate links

Furime services: https://donations.furim.xyz/

Vultr: https://www.vultr.com/?ref=8867604-6G(u get 100$ i get 25$)



